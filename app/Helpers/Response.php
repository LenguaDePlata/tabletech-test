<?php

namespace App\Helpers;

class Response 
{

	protected static $response = null;

	protected $code = 200;
	protected $data = [];

	protected function __construct() {}

	public static function getInstance()
	{
		if (static::$response === null)
		{
			static::$response = new Response(); 
		}

		return static::$response;
	}

	public static function destroy()
	{
		unset(static::$response);
		static::$response = null;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function get()
	{
		$result = [];

		switch($this->code)
		{
			case 200:
			case 204:
				$result = [
					'status' => [
						'message' => 'OK',
						'code' => $this->code
					],
					'data' => $this->data
				];
				break;

			default:
				$result = [
					'status' => [
						'message' => 'ERROR',
						'code' => $this->code
					],
					'errors' => $this->data
				];
				break;
		}

		return $result;
	}
}