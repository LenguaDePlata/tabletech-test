<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Log;
use App\Helpers\Response;

trait HttpPlatform {

	protected $client;
	protected $settings = [
		'default' => []
	];

	public function _initializeClient()
    {
        $this->client = new Client($this->settings['default']);
    }

	/**
	 * Make an API call to a third party data platform
	 *
	 * @param string $url
	 * @param array  $options
	 * @param string $method Request method (default POST)
	 * @param int    $tries If a RequestException is received, number of times
	 *                      we must try again to coonect to the platform and
	 *                      perform the request (default 1)
	 *
	 * @return array|false
	 */
	protected function makeApiCall($url, $options = array(), $method = 'post', $tries = 1)
	{
		$response = Response::getInstance();

	    try {
	    	$resp = null;
	    	if (strtolower($method) == 'post')
	    	{
	        	$resp = $this->client->post($url, $options);
	        }
	        else if (strtolower($method) == 'get')
	        {
	        	$resp = $this->client->get($url, $options);
	        }
	        $response->setCode(200);

	        return $resp;
	    }
	    catch (\GuzzleHttp\Exception\ServerException $e)
	    {
	    	$response->setCode(500);
	        Log::error('Guzzle Server Exception:' . $e->getMessage(), ['api_url' => $url, 'api_options' => print_r($options, true)]);
	        
	        $tries--;
	    	if ($tries > 0)
	    	{
	    		Log::info('Server error. Sleeping for 1 second before trying again (' . $tries . ' tries).');
	    		sleep(1);
	    		return $this->makeApiCall($url, $options, $method, $tries);
	    	}
	    }
	    catch (\GuzzleHttp\Exception\ClientException $e)
	    {
	    	$response->setCode(500);
	        Log::error('Guzzle Client Exception:' . $e->getMessage(), ['api_url' => $url, 'api_options' => print_r($options, true)]);
	    }
	    catch (\GuzzleHttp\Exception\ParseException $e)
	    {
	    	$response->setCode(500);
	        Log::error('Guzzle Parse Exception:' . $e->getMessage(), ['api_url' => $url, 'api_options' => print_r($options, true)]);
	    }
	    catch (\GuzzleHttp\Exception\RequestException $e)
	    {
	    	$response->setCode(400);
	    	Log::error('Guzzle Request Exception:' . $e->getMessage(), ['api_url' => $url, 'api_options' => print_r($options, true)]);
	    	$tries--;
	    	if ($tries > 0)
	    	{
	    		Log::info('Request error. Sleeping for 1 second before trying again (' . $tries . ' tries).');
	    		sleep(1);
	    		return $this->makeApiCall($url, $options, $method, $tries);
	    	}
	    }
	    catch (\Exception $e)
	    {
	    	$response->setCode(500);
	        Log::error('Guzzle Generic Exception:' . $e->getMessage(), ['api_url' => $url, 'api_options' => print_r($options, true)]);
	    }

	    return false;
	}

}