<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PlatformServiceProvider extends ServiceProvider
{
	protected $defer = true;

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('App\Contracts\Platform', 'App\Services\OpenAidService');
	}

	public function provides()
	{
		return ['App\Contracts\Platform'];
	}
}