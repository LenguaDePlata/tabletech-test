<?php

namespace App\Services;

use App\Contracts\Platform;
use App\Helpers\Response;
use App\Traits\HttpPlatform;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use CurrencyConverter\CurrencyConverter as Converter;
use Cache;

class OpenAidService implements Platform
{
	use HttpPlatform;

	const OPENAID_URL = 'http://datastore.iatistandard.org/api/1/access/';
	const TIMEOUT = 30;
	const OFFSET = 500;

	protected $startDate;
	protected $parsedDonations;
	protected $converter;
	protected $currencyTo;

	public function __construct(Converter $converter)
	{
		$this->converter = $converter;
		$this->startDate = date('Y-m-d', mktime(0,0,0,1,1,date('Y')-5));
		$this->endDate = date('Y-m-d');
		$this->initializeSettings();
		$this->_initializeClient();
	}

	protected function initializeSettings()
	{
		$this->settings['default'] = [
			'base_uri'  => self::OPENAID_URL,
			'timeout' 	=> self::TIMEOUT,
			'query' => [
				'recipient-country' => \Config::get('app.donations_country'),
				'start-date__gt'	=> $this->startDate,
				'end-date__lt'		=> $this->endDate
			]
		];
	}

	public function getDonations(Request $request)
	{
		$defaultCurrency = \Config::get('app.default_currency');
		$this->currencyTo = $request->input('currency', $defaultCurrency);
		if (!Cache::has('donations') || Cache::get('currency', $defaultCurrency) != $this->currencyTo)
		{
			$donations = $this->getPaginatedDonations();
			$page = 0;
			$total = $donations['total-count'];
			$this->parsedDonations = [];

			for ($page = 0; $page*self::OFFSET < $total; $page++)
			{ 
				if ($page > 0)
				{
					$donations = $this->getPaginatedDonations($page);
				}
				$this->parseDonations($donations);
			}

			$this->parsedDonations['currency'] = $this->currencyTo;
			Cache::put('donations', $this->parsedDonations, \Config::get('cache.expiration_time'));
			Cache::put('currency', $this->currencyTo, \Config::get('cache.expiration_time'));
		}
		else
		{
			$this->parsedDonations = Cache::get('donations');
		}
		
		return $this->parsedDonations;
	}

	protected function getPaginatedDonations($page = 0)
	{
		$body = [];
		$donations = $this->makeApiCall(
			'activity.json',
			[
				'limit' => 500,
				'offset' => $page*self::OFFSET
			],
			'get', 
			3
		);

		if ($donations !== false)
		{
			$response = $donations->getBody();
			$body = json_decode($response->getContents(), true);
		}

		unset($donations);
		return $body;
	}

	protected function parseDonations($donations)
	{
		foreach ($donations['iati-activities'] as $activity)
		{
			$activity = $activity['iati-activity'];
			$fundingOrganisation = $this->parseFundingOrganisationName($activity);
			$defaultCurrency = $activity['default-currency'];

			if (isset($activity['transaction']))
			{

				foreach ($activity['transaction'] as $transaction)
				{
					$this->parseMonetaryElement($transaction, $fundingOrganisation, $defaultCurrency);
				}
			}
			else
			{
				$this->parseMonetaryElement($activity['budget'], $fundingOrganisation, $defaultCurrency);
			}
		}
	}

	protected function parseFundingOrganisationName($organisations)
	{
		$fundingOrganisation = 'Unknown';
		if (isset($organisations['reporting-org']))
		{
			if (isset($organisations['reporting-org']['text']))
			{
				$fundingOrganisation = $organisations['reporting-org']['text'];
			}
			else if (isset($organisations['reporting-org']['narrative']['text']))
			{
				$fundingOrganisation = $organisations['reporting-org']['narrative']['text'];
			}
		}
		else
		{
			if (!isset($organisations['participating-org'][0]))
			{
				if (isset($organisations['participating-org']['text']))
				{
					$fundingOrganisation = $organisations['participating-org']['text'];
				}
				else if (isset($organisations['participating-org']['narrative']['text']))
				{
					$fundingOrganisation = $organisations['participating-org']['narrative']['text'];
				}
			}
			else
			{
				foreach ($organisations['participating-org'] as $organisation)
				{
					if ($organisation['role'] == 'Funding')
					{
						$fundingOrganisation = $organisation['text'];
						break;
					}
				}
			}
		}

		return $fundingOrganisation;
	}

	protected function parseMonetaryElement($element, $fundingOrganisation, $defaultCurrency)
	{
		if ($element['value']['value-date'] >= $this->startDate &&
			$element['value']['value-date'] <= $this->endDate)
		{
			$year = date('Y', strtotime($element['value']['value-date']));
			if (!isset($this->parsedDonations[$year]))
			{
				$this->parsedDonations[$year] = [];
			}

			if (!isset($this->parsedDonations[$year][$fundingOrganisation]))
			{
				$this->parsedDonations[$year][$fundingOrganisation] = 0;
			}

			$currencyFrom = $defaultCurrency;
			if (isset($element['value']['currency']))
			{
				$currencyFrom = $element['value']['currency'];
			}

			$exchangeRate = 1;
			if (!Cache::has($currencyFrom.'-'.$this->currencyTo))
			{
				$exchangeRate = $this->converter->convert($currencyFrom, $this->currencyTo);
			}
			else
			{
				$exchangeRate = Cache::get($currencyFrom.'-'.$this->currencyTo);
			}
			Cache::add($currencyFrom.'-'.$this->currencyTo, $exchangeRate, 10);

			$exchangedDonationValue = round(intval($element['value']['text'])*$exchangeRate, 2);
			$this->parsedDonations[$year][$fundingOrganisation] += $exchangedDonationValue;
		}
	}

}