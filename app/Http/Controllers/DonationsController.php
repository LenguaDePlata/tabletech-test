<?php

namespace App\Http\Controllers;

use App\Contracts\Platform;
use App\Helpers\Response;

use Illuminate\Http\Request;

class DonationsController extends Controller
{
	
	protected $platform;

	public function __construct(Platform $platform)
	{
		$this->platform = $platform;
	}

	public function index (Request $request)
	{
		$response = Response::getInstance();

		if (!$request->has('errors'))
		{
			$rawResult = $this->platform->getDonations($request);
			if (is_array($rawResult) && empty($rawResult))
			{
				$response->setCode(204);
			}

			$response->setData($rawResult);
		}
		else
		{
			$response->setCode(400);
			$response->setData($request->input('errors'));
		}

		return response()->json($response->get(), $response->getCode());
	}

}