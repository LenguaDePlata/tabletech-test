<?php namespace App\Http\Middleware\Validate;

use Closure;
use Auth;
use Log;
use Illuminate\Validation\Validator;

abstract class AbstractValidateRequestParameters {

	/**
	 * Handle an incoming view request and check whether its params are valid or not.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$requestParameters = $request->all();
		
		Log::info(
			'API connection.'
			.PHP_EOL
			.'API URI: /'.$request->path()
			.PHP_EOL
			.'Request parameters: '.json_encode($requestParameters)
			.PHP_EOL
			.'----------------------------------------------'
		);

		// Define the validation ruleset
		$rules = $this->getValidationRules();

		// Define the custom validation error messages
		$messages = $this->getValidationMessages();

		// Validate the $request input against $rules with $messages
		$validator = \Validator::make($request->all(), $rules, $messages);
		
		// If it fails, we concatenate the error messages to the request
		// and we give it back to the controller
		if ($validator->fails())
		{
			$errors = $validator->messages();
			$request->merge(['errors' => $errors->toArray()]);
		}

		return $next($request);
	}

	/**
	 * Return the list of laravel validation rules for each of the possible fields the
	 * request can contain
	 *
	 * @return array List of Laravel validation rules
	 */
	abstract protected function getValidationRules();

	/**
	 * Return the list of laravel error messages for the different rules and fields
	 * that can be invalidly set which need their own explanation fo the error
	 *
	 * @return array List of Laravel error messages and replacers
	 */
	abstract protected function getValidationMessages();
}