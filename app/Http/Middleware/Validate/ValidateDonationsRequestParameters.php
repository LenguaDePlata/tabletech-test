<?php

namespace App\Http\Middleware\Validate;

class ValidateDonationsRequestParameters extends AbstractValidateRequestParameters
{

	/**
	 * {@inheritdoc}
	 **/
	protected function getValidationRules()
	{
		return [
			'currency' => ['sometimes', 'regex:/[A-Z]{3}/']
		];
	}

	/**
	 * {@inheritdoc}
	 **/
	protected function getValidationMessages()
	{
		return [
			'currency.regex' => 'ISO codes must be three characters long, letters from A to Z only, and all caps'
		];
	}
}