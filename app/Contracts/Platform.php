<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface Platform {

	public function getDonations(Request $request);
	
}