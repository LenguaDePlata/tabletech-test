# OpenAid Tabletech test API

This API implements the technical test for the TableTech application. It connects to the OpenAid API and retrieves and aggregates the monetary aid information donated to the country of Sudan for the last five years by governments, NGO, and other agencies.

It is built with Laravel 5.2 and PHP 5.6.19 and it makes use of GuzzleHttp and Ujjwal's Currency Converter vendors, the former to manage the HTTP connections to retrieve the content from OpenAid, and the latter for exchanging monetary amounts between different currencies.

## Use

This API has been built and tested for a hypothetic host called "api.openaidtech.net"; it sould be created and assigned to localhost as a virtual host in order to test the application. Additionally, the Dotenv file provided, ".env.testing", must be renamed to ".env" ; it will become the environment for the test of the Laravel application.

The API has only one call:

### GET `/donations{?currency}`

> Retrieves and aggregates by year and organisation the aid donations made to Sudan registered by OpenAid for the last five years from today. The oldest year of the aggregation is always considered from January 1st of that year, not from the day its corresponds to five years ago.
> 
> On success the response will have a 200 status code and the body will contain the aggregation, first by year and then by organisation, of all the monetary contributions listed in OpenAid for the country of Sudan. Otherwise, if no results are found, it will return a 204 status code.
> 
> In case wrong parameters are used to make the API call, the response will include a 400 error status code. In case any other errors are found while making the requests to OpenAid, the respnse will include a 500 error status code.
> 
> The API will cache the results returned by OpenAid and aggregated by itself for a period of 12 hours, unless the results are requested in a different currency than the one before.
>
> **Parameters**
>
> > _currency_ `string` (optional) **Default:** EUR
> > > ISO 4217 code of the currency we want the monetary donations aggregated in the response to be represented in. It will exchange all amounts to this currency.
>
> **Response**
>
> > Headers
> >
> > `Content-Type: application/json`
> >
> > Body `200`
> > > ```json
{
	"status":
	{
		"message":"OK",
		"code":200
	},
	"data":
	{
		"2012":
		{
			"Ministry for Foreign Affairs of Finland":8617104,
			"UNICEF":199136373.06
		},
		"2013":
		{
			"Ministry for Foreign Affairs of Finland":29746260,
			"UNICEF":92032419.36
		},
		"2014":
		{
			"Ministry for Foreign Affairs of Finland":2947680,
			"UN Women":5786851.08,
			"UNICEF":2265681.12
		},
		"2015":
		{
			"UN-Habitat":64251971.88,
			"UNICEF":-259981.5
		},
		"2016":
		{
			"UNICEF":2802329.82
		},
		"currency":"EUR"
	}
}
> > >```
> >
> > Body `400`
> > > ```json
{
	"status":
	{
		"message":"ERROR",
		"code":400
	},
	"errors":
	{
		"currency":["ISO codes must be three characters long, letters from A to Z only, and all caps"]
	}
}
> > >```



